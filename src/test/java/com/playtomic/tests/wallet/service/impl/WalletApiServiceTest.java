package com.playtomic.tests.wallet.service.impl;

import com.playtomic.tests.wallet.model.dto.WalletChargeDto;
import com.playtomic.tests.wallet.model.entity.Wallet;
import com.playtomic.tests.wallet.repository.WalletRepositoryCustom;
import com.playtomic.tests.wallet.repository.impl.WalletRepositoryCustomImpl;
import com.playtomic.tests.wallet.service.WalletApiService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WalletApiServiceTest {

    @InjectMocks
    WalletApiServiceImpl walletApiService;
    @Mock
    WalletRepositoryCustom walletRepositoryCustom;


    @Test
    void testGetWallet() {

        Wallet wallet = Wallet.builder()
                .id(1L)
                .currentBalance(BigDecimal.valueOf(20.35))
                .build();

        when(walletRepositoryCustom.getWalletById(anyLong())).thenReturn(wallet);

        Wallet result = walletApiService.getWallet(1L);
        assertSame(wallet, result);

    }

    @Test
    void testUpdateWallet() {

        WalletChargeDto walletChargeDto = WalletChargeDto.builder()
                .creditCardNumber("1234 4567 8976 5678")
                .currentBalance(BigDecimal.valueOf(20.65))
                .build();

        doNothing().when(walletRepositoryCustom).updateCurrentBalance(anyLong(), any(BigDecimal.class));
        walletApiService.updateWallet(1L, walletChargeDto);
        verify(walletRepositoryCustom, times(1))
                .updateCurrentBalance(anyLong(), any(BigDecimal.class));
    }
}
