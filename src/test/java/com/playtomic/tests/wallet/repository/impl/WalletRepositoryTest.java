package com.playtomic.tests.wallet.repository.impl;

import com.mongodb.client.result.UpdateResult;
import com.playtomic.tests.wallet.model.entity.Wallet;
import com.playtomic.tests.wallet.repository.WalletRepositoryCustom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WalletRepositoryTest {

    @InjectMocks
    WalletRepositoryCustomImpl walletRepositoryCustom;
    @Mock
    MongoTemplate mongoTemplate;
    @Mock
    UpdateResult updateResult;

    @Test
    void testGetWalletById() {

        Wallet wallet = Wallet.builder()
                .id(1L)
                .currentBalance(BigDecimal.valueOf(20.35))
                .build();

        when(mongoTemplate.findById(any(Object.class), any(Class.class))).thenReturn(wallet);

        Wallet result = walletRepositoryCustom.getWalletById(1L);

        assertSame(wallet, result);
    }

    @Test
    void testUpdateCurrentBalance() {

        when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), any(Class.class))).thenReturn(updateResult);
        walletRepositoryCustom.updateCurrentBalance(1L, BigDecimal.valueOf(52.35));
        verify(mongoTemplate, times(1))
                .updateFirst(any(Query.class), any(Update.class), any(Class.class));
    }
}
