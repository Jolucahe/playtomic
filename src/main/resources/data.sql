DROP TABLE IF EXISTS WALLET;

CREATE TABLE WALLET (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    CURRENT_BALANCE DECIMAL(5,2) NOT NULL
);

INSERT INTO WALLET (ID, CURRENT_BALANCE) VALUES (1, 25.35);
INSERT INTO WALLET (ID, CURRENT_BALANCE) VALUES (2, 25.36);