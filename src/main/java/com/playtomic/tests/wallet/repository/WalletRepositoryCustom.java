package com.playtomic.tests.wallet.repository;

import com.playtomic.tests.wallet.model.entity.Wallet;

import java.math.BigDecimal;

public interface WalletRepositoryCustom {
    Wallet getWalletById(Long id);
    void updateCurrentBalance(Long id, BigDecimal currentBalance);
}
