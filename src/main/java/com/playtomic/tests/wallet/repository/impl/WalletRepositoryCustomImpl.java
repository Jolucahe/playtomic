package com.playtomic.tests.wallet.repository.impl;

import com.mongodb.client.result.UpdateResult;
import com.playtomic.tests.wallet.model.entity.Wallet;
import com.playtomic.tests.wallet.repository.WalletRepositoryCustom;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
@AllArgsConstructor
public class WalletRepositoryCustomImpl implements WalletRepositoryCustom {

    MongoTemplate mongoTemplate;

    @Override
    public Wallet getWalletById(Long id) {
        return mongoTemplate.findById(id, Wallet.class);
    }

    @Override
    public void updateCurrentBalance(Long id, BigDecimal currentBalance) {

        Query query = new Query(Criteria.where("id").is(id));
        Update update = new Update();
        update.set("currentBalance", currentBalance);

        mongoTemplate.updateFirst(query, update, Wallet.class);

    }
}
