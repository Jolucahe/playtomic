package com.playtomic.tests.wallet.model.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Data
@Builder
@Document(collection = "wallet")
public class Wallet {

    @Id
    private Long id;
    private BigDecimal currentBalance;

}
