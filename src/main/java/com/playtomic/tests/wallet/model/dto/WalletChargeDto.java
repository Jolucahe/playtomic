package com.playtomic.tests.wallet.model.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class WalletChargeDto {
    private String creditCardNumber;
    private BigDecimal currentBalance;
}
