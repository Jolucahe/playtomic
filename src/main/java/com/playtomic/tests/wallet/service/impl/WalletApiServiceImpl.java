package com.playtomic.tests.wallet.service.impl;

import com.playtomic.tests.wallet.model.dto.WalletChargeDto;
import com.playtomic.tests.wallet.model.entity.Wallet;
import com.playtomic.tests.wallet.repository.WalletRepositoryCustom;
import com.playtomic.tests.wallet.service.WalletApiService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class WalletApiServiceImpl implements WalletApiService {

    private WalletRepositoryCustom walletRepositoryCustom;

    @Override
    public Wallet getWallet(Long walletId) {
        return walletRepositoryCustom.getWalletById(walletId);
    }

    @Override
    public void updateWallet(Long walletId, WalletChargeDto walletChargeDto) {
        walletRepositoryCustom.updateCurrentBalance(walletId, walletChargeDto.getCurrentBalance());
    }
}
