package com.playtomic.tests.wallet.service;

public class StripeServiceException extends RuntimeException {

    public StripeServiceException() {
        super();
    }

    public StripeServiceException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
