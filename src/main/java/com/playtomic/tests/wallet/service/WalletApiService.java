package com.playtomic.tests.wallet.service;

import com.playtomic.tests.wallet.model.dto.WalletChargeDto;
import com.playtomic.tests.wallet.model.entity.Wallet;

public interface WalletApiService {
    Wallet getWallet(Long walletId);
    void updateWallet(Long walletId, WalletChargeDto walletChargeDto);
}
