package com.playtomic.tests.wallet.service;

public class StripeAmountTooSmallException extends StripeServiceException {

    StripeAmountTooSmallException() {
        super();
    }

    StripeAmountTooSmallException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
