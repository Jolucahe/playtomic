package com.playtomic.tests.wallet.api;

import com.playtomic.tests.wallet.model.dto.WalletChargeDto;
import com.playtomic.tests.wallet.model.entity.Wallet;
import com.playtomic.tests.wallet.service.WalletApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class WalletController {
    private Logger log = LoggerFactory.getLogger(WalletController.class);

    @Autowired
    private WalletApiService walletApiService;

    @RequestMapping("/")
    void log() {
        log.info("Logging from /");
    }

    @GetMapping("/wallet/{walletId}")
    public ResponseEntity<Wallet> getWallet(@Valid @PathVariable Long walletId) {
        return ResponseEntity.ok(walletApiService.getWallet(walletId));
    }

    @PutMapping("/wallet/{walletId}")
    public ResponseEntity<Void> updateWallet(@Valid @PathVariable Long walletId, @Valid @RequestBody WalletChargeDto walletChargeDto) {
        walletApiService.updateWallet(walletId, walletChargeDto);
        return ResponseEntity.noContent().build();
    }
}
